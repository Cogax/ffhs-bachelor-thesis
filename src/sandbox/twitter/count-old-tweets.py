from TwitterAPI import TwitterAPI, TwitterPager
from tinydb import TinyDB, Query
import json

API_KEY = '2AiZdpD4PDhC7dJVgSp5qTTfE'
API_SECRET = 'SvpawL9kUZ8Bjd5be387OYyZsFYrcePfwxw5XPdbN7xRSRab60'
ACCESS_TOKEN = '33181877-ALDJtv0Uncbz7Jd7ufET8B3oHUkKlFwOBufrXgmVK'
ACCESS_TOKEN_SECRET = 'DakoyqWY6kBusy0A6VzaoEpjctah214UlkRabUk9okFda'

WORDS_TO_COUNT = ['MaidSafeCoin']

api = TwitterAPI(API_KEY, API_SECRET, ACCESS_TOKEN, ACCESS_TOKEN_SECRET)
words = ' OR '.join(WORDS_TO_COUNT)
counts = dict((word,0) for word in WORDS_TO_COUNT)

db = TinyDB('./data/MaidSafeCoin.json')

def to_JSON(obj):
  return json.loads(json.dumps(obj, default=lambda o: o.__dict__))

def process_tweet(tweet):
  db.insert(to_JSON(tweet))
  text = tweet['text'].lower()
  for word in WORDS_TO_COUNT:
    if word in text:
      counts[word] += 1
  print(counts)

r = TwitterPager(api, 'search/tweets', {'q':words, 'count':100})
for item in r.get_iterator(wait=6):
  if 'text' in item:
    process_tweet(item)
  elif 'message' in item and item['code'] == 88:
    print('\n*** SUSPEND, RATE LIMIT EXCEEDED: %s\n' % item['message'])
    break