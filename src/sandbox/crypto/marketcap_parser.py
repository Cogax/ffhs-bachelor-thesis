from bs4 import BeautifulSoup
import urllib2
import csv

filepath_in = "./data/coinmarketcap_coins_28_oct_2018.html"
filepath_out = "./data/coinmarketcap_coins_28_oct_2018.csv"

file = open(filepath_in, 'r')
html = file.read()
file.close()

soup = BeautifulSoup(html)
table = soup.select_one("table.table")

# python3 just use th.text
headers = [th.text.encode("utf-8").strip().replace('\r', '').replace('\n', '') for th in table.select("tr th")[:-1]]

with open(filepath_out, "w") as f:
    wr = csv.writer(f, delimiter=';')
    wr.writerow(headers)
    wr.writerows([[td.text.encode("utf-8").strip().splitlines()[-1] for td in row.find_all("td")[:-1]] for row in table.select("tr + tr")])