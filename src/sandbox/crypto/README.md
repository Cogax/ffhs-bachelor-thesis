# Grab Cryptocurrency Data

## End of day prices
The following command will write a data file to the `data` folder with daily prices configured inside the python file.
* `python grab_end_of_day_prices.py`
