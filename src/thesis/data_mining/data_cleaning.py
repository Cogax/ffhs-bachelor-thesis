import re
from string import digits

def filter_language(df, lang='en'):
    print("Filter dataset for language '{0}'".format(lang))
    filtered_en = df.loc[df['Language'] == "en"]
    filtered_others = df[~df['ID'].isin(filtered_en['ID'])]
    print("\tFiltered out {0} non english tweets".format(filtered_others.shape[0]))
    print("\tActual size: {0}".format(filtered_en.shape[0]))

    return filtered_en, filtered_others


def noise_reduction(df):
    print("Reduce noise on dataset")

    copy = df.copy()

    # Remove all non alphanumeric characters inside 'Text' column
    copy['Text'] = copy['Text'].apply(clean_text)

    # Drop duplicates
    cleaned_copy = copy.drop_duplicates(subset="Text", keep=False, inplace=False)

    cleaned = df[df['ID'].isin(cleaned_copy['ID'])]
    duplicates = df[~df['ID'].isin(cleaned_copy['ID'])]

    print("\tFiltered out {0} duplicates".format(duplicates.shape[0]))
    print("\tActual size: {0}".format(cleaned.shape[0]))

    return cleaned, duplicates, copy


def clean_text(text):
    remove_digits = str.maketrans('', '', digits)

    # lowercase
    text = str(text).lower()

    # remove all links
    text = re.sub(r'''(?i)\b((?:https?://|www\d{0,3}[.]|[a-z0-9.\-]+[.][a-z]{2,4}/)(?:[^\s()<>]+|\(([^\s()<>]+|(\([^\s()<>]+\)))*\))+(?:\(([^\s()<>]+|(\([^\s()<>]+\)))*\)|[^\s`!()\[\]{};:'".,<>?«»“”‘’]))'''," ", text)

    # remove all non alphanummeric
    text = re.sub(r'\W+', ' ', text)

    # remove all digits
    text = text.translate(remove_digits)

    return text.strip()
