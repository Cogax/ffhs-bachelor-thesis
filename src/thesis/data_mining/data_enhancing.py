import numpy as np


def write_id_file(df, filename):
    np.savetxt(filename, df['ID'].values, fmt='%d')