Sentiment Analysis von Kryptowährungs-Tweets
- - - - - - - - - - - - - - - - - - - - - - -
Datum: 10. März 2019
Autor: Andreas Gyr


In diesem Archiv befinden sich einige Dateien, welche die Ausführung der Analysen der Bachelor Thesis beschreiben. Die Dateien sind als Python Jupyter Notebooks (*.ipynb) vorhanden. Alle Notebooks können nur gelesen werden, da für die Ausführung einerseits installierte pip Packete benötigt werden. Andererseits werden Modelle, grosse Datensätze etc. geladen welche sich nicht in diesem Archiv befinden.

Alle Notebooks sind im Notiz-Format abgelegt. Es kann sein, dass darin nicht der aktuellste Stand vorhanden ist. Das Vorgehen, welches in der Thesis beschrieben wird gilt als finale Version.

Die Dateien Exe01*.ipynb bis Exe06*.ipynb wurden für die Analysen verwendet und zeigen die Auswertungen, welche unter Anderem im Kapitel “Ergebnisse” der Thesis präsentiert werden. Die Datei Word2VecModel.ipynb zeigt das erstellen den Word2Vec- sowie Machine Learning Sentiment Klassifikationsmodelles. Die Datei Index.ipynb wurde wärend den Thesis verwendet um das gelernte und das Vorgehen festzuhalten.