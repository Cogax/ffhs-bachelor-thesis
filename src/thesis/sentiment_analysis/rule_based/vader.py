from vaderSentiment.vaderSentiment import SentimentIntensityAnalyzer


def vader_sentiment(df):
    analyser = SentimentIntensityAnalyzer()

    for index, tweet in df.iterrows():
        score = analyser.polarity_scores(tweet['full_text'])
        df.loc[index, 'vader_sentiment'] = score['compound']
        df.loc[index, 'vader_sentiment_class'] = get_vader_sentiment_class(score)

    return df


def get_vader_sentiment_class(score):
    if score['compound'] >= 0.05:
        return 1
    if -0.05 < score['compound'] < 0.05:
        return 0
    if score['compound'] <= -0.05:
        return -1

    return 0