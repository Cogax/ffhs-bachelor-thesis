from afinn import Afinn


def afinn_sentiment(df):
    afinn = Afinn(emoticons=True)

    for index, tweet in df.iterrows():
        score = afinn.score(tweet['full_text'])
        df.loc[index, 'afinn_sentiment'] = score
        df.loc[index, 'afinn_sentiment_class'] = get_afinn_sentiment_class(score)

    return df


def get_afinn_sentiment_class(score):
    if score > 0:
        return 1
    if score < 0:
        return -1

    return 0
