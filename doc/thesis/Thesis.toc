\select@language {english}
\select@language {ngerman}
\select@language {english}
\select@language {ngerman}
\contentsline {chapter}{\numberline {1}Einleitung}{1}{chapter.1}
\contentsline {section}{\numberline {1.1}Zielsetzung}{1}{section.1.1}
\contentsline {section}{\numberline {1.2}Stand der Forschung}{2}{section.1.2}
\contentsline {section}{\numberline {1.3}Problemstellung}{3}{section.1.3}
\contentsline {section}{\numberline {1.4}Ein- und Abgrenzung der Arbeit}{3}{section.1.4}
\contentsline {section}{\numberline {1.5}Aufbau der Arbeit}{4}{section.1.5}
\contentsline {chapter}{\numberline {2}Grundlagen}{5}{chapter.2}
\contentsline {section}{\numberline {2.1}Kryptow\IeC {\"a}hrungen}{5}{section.2.1}
\contentsline {section}{\numberline {2.2}Opinion Mining}{6}{section.2.2}
\contentsline {section}{\numberline {2.3}Sentiment Analysis}{7}{section.2.3}
\contentsline {subsection}{\numberline {2.3.1}Regelbasierte Sentiment Analysis}{7}{subsection.2.3.1}
\contentsline {subsection}{\numberline {2.3.2}Machine Learning Sentiment Analysis}{8}{subsection.2.3.2}
\contentsline {section}{\numberline {2.4}Vektorrepr\IeC {\"a}sentationen von Texten}{9}{section.2.4}
\contentsline {subsubsection}{\nonumberline One-hot-Codierung}{10}{section*.5}
\contentsline {subsubsection}{\nonumberline Frequenzbasierte Codierungen}{10}{section*.6}
\contentsline {subsubsection}{\nonumberline Worteinbettungen}{12}{section*.9}
\contentsline {section}{\numberline {2.5}Korrelation}{14}{section.2.5}
\contentsline {chapter}{\numberline {3}Methodik}{15}{chapter.3}
\contentsline {section}{\numberline {3.1}Data Mining}{16}{section.3.1}
\contentsline {subsection}{\numberline {3.1.1}Datensammlung des Litecoin-W\IeC {\"a}hrungskurses}{16}{subsection.3.1.1}
\contentsline {subsection}{\numberline {3.1.2}Datensammlung der Tweets}{16}{subsection.3.1.2}
\contentsline {subsection}{\numberline {3.1.3}Datenvorbereitung f\IeC {\"u}r die Sentiment Analysis}{18}{subsection.3.1.3}
\contentsline {subsubsection}{\nonumberline Sprachfilterung}{18}{section*.14}
\contentsline {subsubsection}{\nonumberline Noise reduction}{18}{section*.15}
\contentsline {section}{\numberline {3.2}Regelbasiertes Sentiment-Klassifikationsmodell}{19}{section.3.2}
\contentsline {subsection}{\numberline {3.2.1}Klassifizierung der Tweets}{20}{subsection.3.2.1}
\contentsline {section}{\numberline {3.3}Machine Learning Sentiment-Klassifikationsmodell}{20}{section.3.3}
\contentsline {subsection}{\numberline {3.3.1}\IeC {\"U}bersicht}{21}{subsection.3.3.1}
\contentsline {subsection}{\numberline {3.3.2}Datenvorbereitung}{22}{subsection.3.3.2}
\contentsline {subsection}{\numberline {3.3.3}Word-Embeddings-Modell mit Word2Vec}{23}{subsection.3.3.3}
\contentsline {subsection}{\numberline {3.3.4}RNN-Sentiment-Klassifikationsmodell}{24}{subsection.3.3.4}
\contentsline {subsubsection}{\nonumberline Embedding Layer}{25}{section*.22}
\contentsline {subsubsection}{\nonumberline Dropout Layer}{26}{section*.23}
\contentsline {subsubsection}{\nonumberline LSTM Layer}{26}{section*.24}
\contentsline {subsubsection}{\nonumberline Dense Layer}{27}{section*.26}
\contentsline {subsection}{\numberline {3.3.5}Modell trainieren}{27}{subsection.3.3.5}
\contentsline {subsection}{\numberline {3.3.6}Klassifizierung der Tweets}{28}{subsection.3.3.6}
\contentsline {section}{\numberline {3.4}Analyse}{29}{section.3.4}
\contentsline {subsection}{\numberline {3.4.1}Aggregation der Tweets}{29}{subsection.3.4.1}
\contentsline {subsection}{\numberline {3.4.2}Korrelationsanalyse}{29}{subsection.3.4.2}
\contentsline {chapter}{\numberline {4}Ergebnisse}{30}{chapter.4}
\contentsline {section}{\numberline {4.1}Data Mining}{30}{section.4.1}
\contentsline {section}{\numberline {4.2}Machine Learning Sentiment-Klassifikationsmodell}{31}{section.4.2}
\contentsline {subsubsection}{\nonumberline Word2Vec-Modell}{31}{section*.30}
\contentsline {subsection}{\numberline {4.2.1}Validierung des Sentiment-Klassifikationsmodells}{31}{subsection.4.2.1}
\contentsline {section}{\numberline {4.3}Sentiment Analysis Resultate}{32}{section.4.3}
\contentsline {subsection}{\numberline {4.3.1}Regelbasierte Sentiment-Klassifikation}{32}{subsection.4.3.1}
\contentsline {subsection}{\numberline {4.3.2}Machine Learning Sentiment-Klassifikation}{33}{subsection.4.3.2}
\contentsline {subsection}{\numberline {4.3.3}Gegens\IeC {\"a}tzlich Klassifizierte Tweets}{33}{subsection.4.3.3}
\contentsline {section}{\numberline {4.4}Korrelationsanalyse}{34}{section.4.4}
\contentsline {subsection}{\numberline {4.4.1}Korrelation ohne Sentiment-Klassifikation}{35}{subsection.4.4.1}
\contentsline {subsection}{\numberline {4.4.2}Korrelation mit regelbasierter Sentiment-Klassifikation}{36}{subsection.4.4.2}
\contentsline {subsection}{\numberline {4.4.3}Korrelation mit Machine-Learning Sentiment-Klassifikation}{37}{subsection.4.4.3}
\contentsline {chapter}{\numberline {5}Diskussion}{38}{chapter.5}
\contentsline {section}{\numberline {5.1}Data Mining}{38}{section.5.1}
\contentsline {section}{\numberline {5.2}Sentiment Analysis}{38}{section.5.2}
\contentsline {section}{\numberline {5.3}Korrelationsanalyse zwischen Tweets und W\IeC {\"a}hrungskurs}{39}{section.5.3}
\contentsline {section}{\numberline {5.4}Fazit}{40}{section.5.4}
\contentsline {section}{\numberline {5.5}Ausblick}{40}{section.5.5}
\contentsline {chapter}{\nonumberline Abbildungsverzeichnis}{41}{chapter*.45}
\contentsline {chapter}{\nonumberline Tabellenverzeichnis}{42}{chapter*.46}
\contentsline {chapter}{\nonumberline Auflistungsverzeichnis}{43}{chapter*.47}
\contentsline {chapter}{\nonumberline Literaturverzeichnis}{44}{chapter*.48}
