
\chapter{Grundlagen}
In diesem Kapitel werden die Grundlagen erläutert, die für das Verständnis der weiteren Arbeit notwendig sind. Hier werden Begriffe wie \textit{Opinion Mining} oder \textit{Sentiment Analysis} genauer erklärt. Im weiteren Verlauf der Arbeit werden dann oft die englischen Ausdrücke dafür verwendet.

\section{Kryptowährungen}
Der Kryptowährungsmarkt hat sich im letzten Jahrzehnt stark entwickelt. Seit dem Startschuss von Bitcoin im Jahr 2008 sind zahlreiche weitere Konzepte für digitale Währungen entstanden. Gerade die letzten Jahre waren es jedoch, die für viel Aufmerksamkeit in den Medien gesorgt haben. So ist der Bitcoin-Preis (und mit ihm auch praktisch alle anderen Kryptowährungen) innerhalb des Jahres 2017 um fast das Zwanzigfache gestiegen. Beinahe so rasch wie der Anstieg ausfiel schnellte er nach dem Jahreswechsel zum Jahr 2018 wieder herab. Zum Zeitpunkt dieser Arbeit liegt der Preis für einen Bitcoin bei etwa \$ 4000 \cite{coinmarketcap}.

\begin{figure}[ht]
  \begin{center}
  \includegraphics[width=9cm]{img/litecoin-price}
  \end{center}
  \caption[Preisentwicklung von Bitcoin und Litecoin \cite{cryptocompare}]{Preisentwicklung von Bitcoin und Litecoin in den letzten Jahren \cite{cryptocompare}}
  \label{fig:bitcoin}
\end{figure}

\section{Opinion Mining}\label{ch:opinionmining}
Das World Wide Web hat sich über die Jahre immer weiter in Richtung soziales Medium entwickelt, in dem jeder, der dazu Zugang hat, seine Ideen verwirklichen und mit anderen teilen kann. Dabei entstehen Unmengen an Daten, die die Probleme, Herausforderungen und Erfolge der Gesellschaft widerspiegeln. So haben die Daten der sozialen Medien in der Wissenschaft einen hohen Stellenwert bei der Meinungs- und Entscheidungsfindung erlangt \cite{opinionmining,digitalreport}.

\begin{italicquotes}
  ``Others' opinions can be crucial when it's time to make a decision or choose among multiple options. When those choices involve valuable resources (for example, spending time and money to buy products or services) people often rely on their peers' past experiences.'' \cite[Cambria et al., S. 15]{opinionmining}
\end{italicquotes}

\noindent Unter dem Begriff \textit{``Opinion Mining''} versteht man das Extrahieren von Entscheidungskriterien aus dem World Wide Web. Opinion Mining wird beispielsweise bei politischen Wahlkampagnen oder im Marketingbereich eingesetzt. Dabei werden die Daten der sozialen Medien verarbeitet, und es wird versucht, sie mit Blick auf die entsprechende Problemstellung zu interpretieren. Die Daten werden dabei mit verschiedenen Methoden, meist automatisiert, prozessiert, um wichtige Entscheidungen treffen zu können \cite{opinionmining}.

Die Microblogging-Plattform Twitter ist seit 2006 online und hat bis heute 300 Millionen monatlich aktive Nutzer \cite{digitalreport}. Microblogging-Plattformen zeichnen sich dadurch aus, dass die Nutzer die Möglichkeit haben, eine kurze Nachricht zu veröffentlichen. In Bezug auf Opinion Mining sind solche Plattformen sehr nützlich, da die Nachrichten sehr kurz und präzise formuliert werden müssen \cite{opinionminingtwitter}. Im Fall von Twitter werden die Nachrichten \textit{``Tweets''} genannt, die zusätzlich mit Metainformationen wie \# (Hashtags) oder $@$ (at) versehen werden können. Ersteres stellt dabei einen Bezug zu einem bestimmten Thema her. Mit dem $@$-Symbol können andere Twitter-Nutzer referenziert werden.
Ein weiteres Merkmal, dass Twitter in Bezug auf Opinion Mining so interessant macht, ist, dass die Daten einfach verfügbar sind. Über die Twitter API können beispielsweise Unternehmen oder Forschungsteams gezielt spezifische Daten abfragen. Dabei kann nach unterschiedlichen Metainformationen wie z. B. Geoinformationen gefiltert werden \cite{twitterapi}. Die meisten der bereits vorgestellten, verwandten Arbeiten haben auf Twitter als Datenquelle zugegriffen.

Nebst dem automatisierten Zugriff auf Daten solcher Plattformen wird oft auch das automatisierte Veröffentlichen von Informationen von den Plattformen angeboten. In Bezug auf das Opinion Mining ist dies ein erhebliches Problem, da mit solchen automatisierten Systemen (auch \textit{``Bot's''} genannt) versucht wird, die Meinung anderer Nutzer zu beeinflussen \cite{opinionbots}.

\section{Sentiment Analysis}
Kurzgefasst versucht Sentiment Analysis (deutsch \textit{Stimmungsanalyse}), die Meinung, Gesinnung, das Sentiment oder Emotionen in Texten zu finden \cite{textdatamanagement, surveyoftechniques}. Damit ist Sentiment Analysis dem Opinion Mining unterzuordnen. Sentiment Analysis kann weiter in zwei Teilgebiete unterteilt werden: Das eine, \textit{Polarity Analysis} (auch \textit{Polarity Detection} genannt \cite{opinionmining}), untersucht, ob die Daten eher positiv oder negativ verfasst wurden. Mit \textit{Emotion Analysis} versucht man, das präzise Gefühl hinter einem Text zu charakterisieren, beispielsweise ``fröhlich'', ``überrascht'' oder ``traurig'' \cite{textdatamanagement}. Im weiteren Verlauf dieser Arbeit wird unter Sentiment Analysis und Sentiment-Klassifikation die Zuordnung eines Textes in die Kategorien positiv, negativ oder neutral verstanden. Allgemein wird heute unter dem Begriff Sentiment Analysis sehr oft die eigentliche Polarity Detection verstanden \cite{opinionmining}.

Bei Sentiment Analysis wird NLP (\textit{Natural Language Processing}) eingesetzt. NLP versucht, die menschliche Sprache zu interpretieren und zu vearbeiten \cite{sentimentsocial}. Der Fachbereich von NLP ist sehr umfangreich. Er beinhaltet beispielsweise automatisierte Spracherkennung, Textgenerierung, Sprachübersetzung und auch Sentiment oder Emotion Analysis. Fakt ist, dass NLP eine starke Abhängigkeit zur Sprache hat. Die syntaktischen und semantischen Strukturen unterscheiden sich bei den zahlreichen verschiedenen Sprachen in hohem Masse. In dieser Arbeit werden einige Begriffe aus dem Bereich der NLP benutzt.

Es gibt verschiedene Ansätze, wie Sentiment Analysis durchgeführt werden kann. Die Arbeiten von Ahmet et al. \cite{sentimentsocial} und Thakkar et al. \cite{approachesforsa} geben einen guten Überblick über die verschiedenen Ansätze. Die automatisierten Ansätze werden dabei primär in zwei Kategorien unterteilt. Zum einen gibt es regelbasierte Ansätze, zum anderen Ansätze, die auf Machine Learning basieren. Natürlich können die beiden Ansätze auch kombiniert werden, wobei man von einem hybriden Ansatz spricht. Thakkar et al. haben in ihrer Literaturarbeit die Ansätze gegeneinander abgewogen, wobei Machine-Learning-Ansätze besser performten als die regelbasierten.

\subsection{Regelbasierte Sentiment Analysis}
Wird Sentiment Analysis mit einem regelbasierten Ansatz durchgeführt, so wird ein Text gemäss vordefinierten Regeln bewertet. Ein häufig angewandter Ansatz ist die sogenannte \textit{Lexical Analysis} \cite{approachesforsa}. Dabei wird ein Text mit einem Tokenizer in einzelne Tokens aufgeteilt. Ein Token kann beispielsweise ein Wort sein, wobei man dann von Unigrammen spricht. Diese Tokens werden dann gemäss einem definierten Lexikon bezüglich ihres Sentiments bewertet. Im Fall von Unigrammen wird in diesem Lexikon beispielsweise jedem Wort ein numerischer Wert zugeordnet, der aussagt, in welchem Masse positiv oder negativ das entsprechende Wort ist.

Regelbasierte Ansätze sind grundsätzlich relativ simpel aufgebaut. Der Nachteil des oben genannten lexikonbasierten Ansatz ist, dass Wortkombinationen nicht berücksichtigt werden. Wird ein Unigramm-Tokenizer verwendet, so wird ein Text, der \textit{``nicht gut''} beinhaltet, diese Textstelle als positiv bewerten. Dieses Problem kann grundsätzlich relativ einfach umgangen werden mit der Anwendung eines Bigramm-Tokenizers, der den Text in jeweils Zwei-Wort-Paare aufteilt. Jedoch stossen die regelbasierten Ansätze bei komplexeren Satzstellungen relativ schnell an ihre Grenzen.

\subsection{Machine Learning Sentiment Analysis}
Machine-learning-basierte Ansätze zeichnen sich dadurch aus, dass die Regeln, die zu einer entsprechenden Sentiment-Zuteilung führen, maschinell und von bestehenden Daten erlernt werden. Meist werden \textit{Supervised-Learning-Algorithmen} angewendet, die mit gelabelten Texten das Regelwerk maschinell erlernen. Die Schwierigkeit bei den machine-learning-basierten Ansätzen besteht darin, dass ein Text für die maschinelle Verarbeitung in eine numerische Form gebracht werden muss, damit er optimal verarbeitet werden kann. Normalerweise wird versucht, einen Text in eine numerische Vektorrepräsentation umzuwandeln, bevor er von einem Machine-Learning-Algorithmus prozessiert wird. Im nächsten Kapitel wird näher auf diese Problemstellung eingegangen, und verschiedene Lösungsansätze werden präsentiert.

Wie die Arbeit von Thakkar et al. zeigt, gibt es verschiedene Algorithmen und Modelle, die den Machine-Learning-Ansatz für Sentiment Analysis implementieren. In der Fachliteratur haben sich in den letzten Jahren die Neuronalen Netze gefestigt. Auch für Sentiment-Analysis-, bzw. Sentiment-Klassifikation-Problemstellungen sind Neuronale Netze sehr verbreitet. Meist sind die Netze mehrschichtig, wobei man von \textit{Deep Learning} spricht. Die Arbeit von Zhang et al. \cite{deeplearningsa} veranschaulicht verschiedene Architekturen von Deep-Learning-Netzen im Zusammenhang mit Sentiment Analysis. Unter anderem wird die RNN (\textit{Recurrent Neural Network})-Architektur vorgestellt, die auch in der Arbeit von Semeniuta et al \cite{rnnsa} für die Sentiment Analysis von Tweets verwendet wurde.

\noindent Rekurrente Neuronale Netze (RNN) unterscheiden sich von herkömmlichen \textit{Feed-Forward-Netzen} dadurch, dass sie auch Beziehungen zwischen zeitlich versetzten Eingaben erlernen können. Typischerweise werden RNNs bei sequenziellen Daten eingesetzt. Genau dieser Aspekt macht sie für die Sentiment Analysis und allgemein die Textverarbeitung so interessant. Ein Text ist immer sequenziell aufgebaut, wo ein Wort einen Bezug zu einem vorhergehenden Wort haben kann. Gerade das oben genannte Beispiel \textit{``nicht gut''} veranschaulicht, dass das zweite Wort vom ersten abhängt und in dem Fall völlig anders zu interpretieren ist.

\section{Vektorrepräsentationen von Texten}\label{ch:wordembeddings}
Damit ein Text maschinell verarbeitet werden kann, muss er in einem Format vorliegen, das der entsprechende Algorithmus versteht. Dies ist normalerweise ein numerischer Vektor. Im Kontext von Machine Learning wird aber auch häufig von einem 1-dimensionalen Tensor (entspricht einem Spaltenvektor) gesprochen. Ein Text kann sehr einfach in einer Vektorform dargestellt werden, z. B. als Vektor seiner einzelnen Wörter.

\begin{italicquotes}
  D\textsubscript{1} = ``.. I like a dog ..''\\
  D\textsubscript{2} = ``.. a bird flies ..''\\
  D\textsubscript{3} = ``.. like a bird  ..''\\
  D\textsubscript{4} = ``.. a dog, a bird  ..''
\end{italicquotes}

\noindent Die oben dargestellten Dokumente D\textsubscript{1} bis D\textsubscript{4} lassen sich wie folgt als Vektoren ihrer Wörter darstellen:

\begin{equation}
  u_{D1} = 
  \begin{bmatrix}
    I \\
    like \\
    a \\
    dogs
  \end{bmatrix}
  \,\,\,\,\,\,\,\,\,\,\,\,
  u_{D2} = 
  \begin{bmatrix}
    a \\
    bird \\
    flies
  \end{bmatrix}
  \,\,\,\,\,\,\,\,\,\,\,\,
  u_{D3} = 
  \begin{bmatrix}
    like \\
    a \\
    bird
  \end{bmatrix}
  \,\,\,\,\,\,\,\,\,\,\,\,
  u_{D4} = 
  \begin{bmatrix}
    a \\
    dog \\
    a \\
    bird
  \end{bmatrix}
\end{equation}

\noindent So kann ein Text zwar in einer Form als Vektor dargestellt werden. Für die maschinelle Verarbeitung sind numerische Vektoren jedoch einfacher. Verschiedene Ansätze haben sich dabei etabliert. Dabei wird jeweils versucht, für ein einzelnes Wort eine numerische Skalar- oder Vektorrepräsentation zu finden. Das Dokument wird dann als eine Sequenz dieser Vektoren oder Skalare repräsentiert.

\subsubsection{One-hot-Codierung}
Die \textit{One-hot-Codierung} ist eine binäre Repräsentation der Vorkommnisse einzelner Wörter innerhalb eines Vokabulars. Dazu werden alle Wörter des Datensatzes (auch Korpus genannt) als Vokabular $V$ aufgelistet. Zu jedem Wort wird ein eindeutiger Index $i$ zugeordnet. Der Wortvektor $u$ wird als Binärvektor der Grösse $N$ repräsentiert (wobei $N$ der Anzahl der Wörter im Vokabular entspricht), der nur Nullen enthält -- ausser an Stelle $i$ eine $1$ \cite{kerasbook}.

\begin{equation}
  V = 
  \begin{bmatrix}
    I \\
    a \\
    like \\
    bird \\
    dog \\
    flies
  \end{bmatrix}
  \,\,\,\,\,\,\,\,\,\,\,\,
  u_{dog} = 
  \begin{bmatrix}
    0 \\
    0 \\
    0 \\
    0 \\
    1 \\
    0
  \end{bmatrix}
  \,\,\,\,\,\,\,\,\,\,\,\,
  u_{flies} = 
  \begin{bmatrix}
    0 \\
    0 \\
    0 \\
    0 \\
    0 \\
    1
  \end{bmatrix}
\end{equation}

\noindent Dieser Ansatz führt bei grösseren Datensätzen zu dünnbesetzten und hochdimensionalen Vektoren. Dies stellt eine erhebliche Schwäche dar. Zudem gehen Zusammenhänge zwischenen den einzelnen Wörtern sowie die semantische Bedeutungen einzelner Wörter verloren.

\subsubsection{Frequenzbasierte Codierungen}
Bei frequenzbasierten Codierungen wird das Auftreten eines Wortes gezählt. Das einfachste Modell ist das \textbf{Bag-of-Words-Modell} (\textit{BoW}). Dabei werden alle Wörter mit deren Anzahl an Auftretungen pro Dokument aufgestellt, wie in Tabelle \ref{table:bow} ersichtlich ist.

\begin{table}[ht]
  \begin{center}
  \footnotesize
  \begin{tabular}{SSSSSSS} \toprule
    & {$I$} & {$a$} & {$like$} & {$bird$} & {$dog$} & {$flies$} \\ \midrule
    {$D_{1}$} & 1 & 1 & 1 & 0 & 1 & 0 \\
    {$D_{2}$} & 0 & 1 & 0 & 1 & 0 & 1 \\
    {$D_{3}$} & 0 & 1 & 1 & 1 & 0 & 0 \\
    {$D_{4}$} & 0 & 2 & 0 & 1 & 1 & 0 \\\bottomrule
  \end{tabular}
\end{center}
\caption{Bag of Words Modell angwandt}
\label{table:bow}
\end{table}

\noindent Das BoW-Modell hat gegenüber der One-hot-Codierung zwar Vorteile. Jedoch treten auch hier Probleme auf. Die Vektoren basieren auf der absoluten Anzahl an Erscheinungen. Dies kann dazu führen, dass häufig auftretende Wörter wie \textit{``me''}, \textit{``my''}, \textit{``the''} etc. sehr dichtbesetzte Vektoren erhalten. Wörter, die für die Aussage eines Textes relevanter, jedoch auch seltener sind, können so von diesen dichtbesetzten Vektoren ``überdeckt'' werden. In Bezug auf Maschine-learning ist dies nicht optimal.

Mit dem \textbf{TF-IDF-Modell} (\textit{Term-Frequency Inverse-Document-Frequency-Modell}) kann die Schwäche des BoW Modells adressiert werden. Der Algorithmus stellt die Häufigkeit eines Wortes $t$ innerhalb eines einzelnen Dokumentes $d$ in Relation zur Häufigkeit des Wortes $t$ im gesamten Datensatz $D$ (Korpus) \cite{textdatamanagement}. Dadurch kann zwar das Problem der Überdeckungen von nicht Relevanten Wörtern gelöst werden. Die Zusammenhänge zwischen Wörtern sowie die semantische Bedeutung eines Wortes gehen aber ebenfalls verloren. 

Der TF-IDF-Wert wird berechnet indem man erst den TF-Wert $tf(t,d)$ berechnet. Dieser entspricht der Anzahl an Erscheindungen des Wortes $t$ im Dokument $d$. Der IDF-Wert $idf(t,D)$ setzt sich aus der totalen Anzahl an Dokumenten des gesamten Datensatzes $D$ in Relation zur Anzahl an Dokumenten welche das Wort $t$ enthalten zusammen. Oft wird der IDF-Wert mit einem Logarithmus mit Basis 2 ($\log_2$) errechnet. Der TF-IDF-Wert entspricht schlussendlich der Multiplikation von beiden, also $tf \cdot idf$.

Der TF-IDF-Wert gibt somit Auskunft über die Relevanz eines Wortes, bezogen auf den gesamten Korpus, wobei ein grösserer Wert eine höhere Relevanz des Wortes bedeutet. In Tabelle \ref{table:tfidf} sind die Werte nach Anwendung auf das Beispiel ersichtlich. Die Spalte für das Wort $a$ unterscheidet sich vom BoW-Ansatz massiv, da das Wort eine niedrige Relevanz hat.

\begin{table}[ht]
  \begin{center}
  \footnotesize
  \begin{tabular}{SSSSSSS} \toprule
    & {$I$} & {$a$} & {$like$} & {$bird$} & {$dogs$} & {$flies$} \\ \midrule
    {$D_{1}$} & 2.0 & 0.0 & 1.0 & 0.0 & 1.0 & 0.0 \\
    {$D_{2}$} & 0.0 & 0.0 & 0.0 & 0.4 & 0.0 & 0.0 \\
    {$D_{3}$} & 0.0 & 0.0 & 1.0 & 0.4 & 0.0 & 0.0 \\
    {$D_{4}$} & 0.0 & 0.0 & 0.0 & 0.4 & 1.6 & 0.0 \\\bottomrule
  \end{tabular}
  \end{center}
  \caption{TF-IDF Modell angwandt}
  \label{table:tfidf}
\end{table}

\noindent Angewandt auf das Beispiel sehen die Wortvektoren des BoW- bzw. TF-IDF Modelles ähnlich aus. Der TF-IDF-basierte Vektor $v_{dogs}$ repräsentiert jedoch eine relative Relevanz der Wörter im Bezug auf den gesamten Korpus. Im BoW-basierten Vektor $u_{dogs}$ werden häufig auftretende Wörter automatisch auch eine höhere Gewichtung erhalten, unabhängig von ihrer Relevanz (Wörter wie ``the'', ``and'' etc.). Bedeutungen der Wörter können jedoch mit beiden Modellen nicht repräsentiert werden.

\begin{equation}
  u_{dog} = 
  \begin{bmatrix}
    1 \\
    0 \\
    0 \\
    1
  \end{bmatrix}
  \,\,\,\,\,\,\,\,\,\,\,\,
  v_{dog} = 
  \begin{bmatrix}
    1.0 \\
    0.0 \\
    0.0 \\
    1.6
  \end{bmatrix}
\end{equation}

\subsubsection{Worteinbettungen}
Worteinbettungen (\textit{Word Embeddings}) gehören zu den vorhersagebasierten (\textit{Prediction Based}) Codierungen. Mit Worteinbettungen ist es möglich, dass die Vektorrepräsentation eines Wortes Zusammenhänge zu anderen Wörtern sowie die Bedeutung repräsentieren kann. Dabei werden die Wörter anhand der umgebenden Wörter und ihres Auftretens im Text in einem mehrdimensionalen Raum angeordnet. Wörter, die häufig im selben Kontext auftreten, erhalten somit einen ähnlichen Vektor.

\begin{italicquotes}
  ``Im Gegensatz zu den durch One-hot-Codierung erzeugten Wortvektoren werden Worteinbettungen anhand der Daten erlernt. Worteinbettungen sind nicht selten 256-, 512- oder 1.024-dimensional, wenn das Vokabular entsprechend groß ist. Andererseits ergeben sich bei der One-hot-Codierung oft Vektoren,die 20.000-dimensional (in diesem Fall kann ein Vokabular von 20.000 Wörtern erfasst werden) oder noch größer sind. Worteinbettungen bringen also mehr Informationen in sehr viel weniger Dimensionen unter.'' \cite[Fran\c{c}ois Chollet, S. 238]{kerasbook}
\end{italicquotes}

\begin{italicquotes}
  ``Die geometrischen Beziehungen zwischen Wortvektoren sollten die semantischen Beziehungen zwischen den Wörtern widerspiegeln. Wortvektoren haben die Aufgabe, die menschliche Sprache auf einen geometrischen Raum abzubilden.'' \cite[Fran\c{c}ois Chollet, S. 238]{kerasbook}
\end{italicquotes}

\noindent Worteinbettungs-Modelle werden mit \textit{unsuppervised-learning-Vorgehensweisen} erstellt. Dabei wird von einem sehr umfangreichen Korpus gelernt. Es wird berechnet, wie gross die Wahrscheinlicheit ist, dass das Wort innerhalb eines gewissen Kontextes auftritt. Es gibt verschiedene Implementationen dieses Ansatzes, im Folgenden wird der \textit{Word2Vec-Ansatz} von Google, der im Jahr 2013 von Mikolov et al. \cite{word2vec} vorgestellt wurde, genauer betrachtet.

\pagebreak
\noindent \textbf{Word2Vec}\\
Word2Vec beinhaltet zwei verschiedene Ansätze, die beide mit Neuronalen Netzen trainiert werden. Beim \textbf{Continuous Bag of Words (CBOW)} Ansatz wird versucht, von umfliessenden Wörtern $w(t-n)$ das Zielwort $w(t)$ vorherzusagen. Angenommen, ein Dokument beinhaltet die Wortsequenz \textit{"but there is a dog which is barking loudly"}, so werden die Wörter \textit{``but''}, \textit{``there''}, \textit{``is''}, \textit{``a''}, \textit{``which''}, \textit{``barking''} und \textit{``loudly''} verwendet, um das Wort \textit{``dog''} vorherzusagen. Beim \textbf{Skip-gram} Ansatz werden aufgrund eines Zielwortes $w(t)$ die umfliessenden Wörter $w(t-n)$ vorhergesagt. Auf das Beispiel angewandt, wird das Wort \textit{``dog''} verwendet um \textit{``but''}, \textit{``there''}, \textit{``is''}, \textit{``a''}, \textit{``which''}, \textit{``barking''} und \textit{``loudly''} vorgerzusagen. In~\Fref{fig:cbow-sg} aus der Arbeit von Mikolov et al. sind die beiden Architekturen dargestellt. Bei beiden Ansätzen kann eine maximale Distanz $n$ zwischen den Wörtern angegeben werden, die der Kontext eines Wortes haben soll.

\begin{figure}[ht]
  \begin{center}
  \includegraphics[width=11cm]{img/cbow-sg}
  \end{center}
  \caption[CBOW und Skip-gram Architekturen \cite{word2vec}]{Architektur der Neuronalen Netzte für den CBOW- sowie Skip-gram Ansatz \cite{word2vec}}
  \label{fig:cbow-sg}
\end{figure}

\noindent Für das Training eines Word2Vec-Modells werden Dokumente benötigt, aus denen das Neuronale Netz lernen kann. Die Ausgabeschicht des Netzes weist dabei je nach Ansatz eine Wahrscheinlichkeit zu Ziel- bzw. Kontextwörtern zu. Bedeutungen von Wörtern können nur erlernt werden, wenn diese auch in den Dokumenten des Trainings enthalten sind, weshalb die Wahl der Trainingsdaten relevant ist, um Fachsprachen zu lernen. Wurde das Neuronale Netz gut trainiert, so sind die numerischen Wortvektoren mathematisch gestützt, wie das folgende Zitat veranschaulicht.
\pagebreak
\begin{italicquotes}
  ``[...] Finally, we found that when we train high dimensional word vectors on a large amount of data, the resulting vectors can be used to answer very subtle semantic relationships between words, such as a city and the country it belongs to, e.g. France is to Paris as Germany is to Berlin. [...] To find a word that is similar to small in the same sense as biggest is similar to big, we can simply compute vector X = vector(``biggest'') - vector(``big'') + vector(``small''). Then, we search in the vector space for the word closest to X measured by cosine distance, and use it as the answer to the question (we discard the input question words during this search). When the word vectors are well trained, it is possible to find the correct answer (word smallest) using this method. [...]''
   \cite[Mikolov et al., S. 2]{word2vec}
\end{italicquotes}

\section{Korrelation}\label{ch:korrelation}
In der Statistik wird unter Korrelation ein Zusammenhang zwischen zwei quantitativen Merkmalen verstanden. Dabei gibt ein Korrelationskoeffizient Auskunft über die Stärke des Zusammenhanges. Der Korrelationskoeffizient $r_{xy}$ ist dabei so definiert, dass die Werte zwischen $-1$ und $+1$ liegen, also $-1 \leq r_{xy} \leq +1$. Bei $r_{xy} > 0$ besteht ein gleichsinniger, bei $r_{xy} < 0$ ein gegensinniger Zusammenhang, bzw. eine positive oder eine negative Korrelation. Liegt $r_{xy}$ nahe bei $0$, so besteht kein Zusammenhang bzw. keine Korrelation. Meist wird dazu der Pearson'sche Korrelationskoeffizient (benannt nach dem englischen Mathematiker Karl Pearson) verwendet, welcher Auskunft über den linearen Zusammenhang gibt \cite{teschl}.

Neben dem Pearson'schen Korrelationskoeffizienten wird in der Statistik auch häufig die Korrelation nach Spearman (benannt nach Charles Spearman) verwendet. Bei Spearmans Ansatz müssen die zwei Variablen im Wesentlichen nur eine Reihenfolge beinhalten. Deshalb wird der Ansatz auch als Rangfolgekorrelation bezeichnet. Der Spearman'sche Korrelationskoeffizient ist kein Mass, das die lineare Abhängigkeit misst \cite{correlation}. Die Studie von 
Hauke et al. hat die beiden Ansätze verglichen und kam zum Schluss, dass der Spearman'sche Korrelationskoeffizient nicht überinterpretiert werden darf \cite{correlation}. Die statistische Signifikanz einer Korrelation kann beispielsweise mit dem p-Wert bestimmt werden.