\section{Data Mining}\label{ch:method-data-mining}
Damit die bevorstehenden Analysen durchgeführt werden können, müssen die Daten dazu erst gesammelt bzw. aufgezeichnet und dann entsprechend aufbereitet werden. Dies betrifft die Währungskursdaten von Litecoin sowie die Tweets von Twitter. Die Zeitspanne ist 13. November 2018 bis 20. Februar 2019.

\subsection{Datensammlung des Litecoin-Währungskurses}
Die Daten des Währungskurses von Litecoin sind über die CryptoCompare API \cite{cryptocompare} zu erreichen. Dazu muss keine Aufzeichnung der Daten gestartet werden, da der Zugang zu historischen Daten zur Verfügung steht.

\begin{figure}[ht]
  \begin{center}
  \includegraphics[width=6cm]{img/crypto-mining}
  \end{center}
  \caption{Schematische Darstellung des Kryptowährungs-Data-Mining}
  \label{fig:cryptomining}
\end{figure}

\noindent Die API kann mit verschiedenen Parametern konfiguriert werden, die Auswirkungen auf den Detaillierungsgrad der gelieferten Daten haben. Für diese Arbeit werden die historischen Daten auf täglichem Datenintervall abgefragt. Als Vergleichswährung wird die amerikanische Währung US-Dollar (USD) gewählt.\\

\begin{minipage}{\linewidth}
  \begin{lstlisting}[style=py,caption={API Endpunkt für historische Litecoin-Währungskurs-Daten},label={lst:cryptoapi}]
    url = "https://min-api.cryptocompare.com/data/histoday?fsym=LTC&tsym=USD"
  \end{lstlisting}
\end{minipage}

\noindent Die gelieferten Daten beinhalten verschiedene Währungskursdaten. Es sind Daten zum Handelsvolumen sowie Tages-, Öffnungs- und Schlusskurs vorhanden. Im Datensatz für diese Arbeit wird jedoch nur der Tagesschlusskurs beachtet, wie dies auch in den meisten Arbeiten, die in Kapitel~\ref{ch:forschungsstand} vorgestellt wurden, getan wurde.

\subsection{Datensammlung der Tweets}
Die Tweets werden, im Gegenasatz zu den Währungsdaten, nicht auf einmal historisch bezogen. Vielmehr werden sie ab dem Startzeitpunkt des oben definierten Zeitraums aufgezeichnet. Dazu werden die Funktionalitäten der Twitter API's \cite{twitterapi} genutzt. Für das automatisierte Aufzeichnen über die Twitter API wird der Online-Service von Tweet Archivist \cite{tweetarchivist} genutzt, bei dem eine Abfrage in Form eines Querys hinterlegt werden kann. Diese Vorgehensweise wird von Chatterjee et al. \cite{twitterminingtactics} vorgestellt und ebenfalls in den Arbeiten von Kaminski et al. \cite{nowcasting} sowie Batool et al. \cite{precisetweet} angewandt. Tweet Archivist erlaubt es, ein API Query zu hinterlegen, das dann täglich automatisch ausgeführt wird. Die Tweets, die dem Query entsprechen, werden gesammelt und in einer Datei abgespeichert. Das Query kann dabei einige Filtermechanismen der Twitter API beinhalten, womit nach Metadaten wie Username, Spache etc. gefiltert werden kann.

\begin{figure}[ht]
  \begin{center}
  \includegraphics[width=8cm]{img/tweet-mining}
  \end{center}
  \caption[Schematische Darstellung des Tweet Mining]{Schematische Darstellung des Tweet Mining}
  \label{fig:tweetmining}
\end{figure}

\noindent Das verwendete Query wurde gemäss den verwandten Arbeiten definiert und beinhaltet lediglich den Namen der Kryptowährung. Dadurch werden alle Tweets, die die Zeichenkette \textit{``Litecoin''} enthalten, aufgezeichnet und gespeichert. Somit werden auch Tweets, die die Kryptowährung als Hashtag (\#Litecoin) vermerkt haben, aufgezeichnet. Das Query wird bewusst nicht mit dem Symbol bzw. der Kurzschreibweise von Litecoin (\textit{``LTC''}) ergänzt, da diese Zeichenkette nicht eindeutig genug ist (Firmennamen etc.). Solche Problemstellungen könnten jedoch mit den erweiterten Filterkriterien der Twitter API gelöst werden \cite{twitterapi}.

Die Daten, die Tweet Archivist aufgrund des definierten Querys zur Verfügung stellt, beinhalten einige Metainformationen wie Tweet-ID, Username, Datum, Zeit und den Inhalt des Tweets. Leider sind die Tweets auf eine bestimmte Zeichenlänge begrenzt, was die von Tweet Archivist gelieferten Tweet-Texte unbrauchbar macht. Mithilfe der konkreten Tweet-ID und der Twitter API können einzelne Tweets per ID jedoch relativ einfach abgefragt werden. In~\Fref{fig:tweetmining} ist das Vorgehen schematisch dargestellt. Die Tweets müssen also einzeln per ID über die Twitter API geladen werden.\\

\begin{minipage}{\linewidth}
  \begin{lstlisting}[style=py,caption={Twitter API Query},label={lst:twitterapiquery}]
    query = "litecoin"
  \end{lstlisting}
\end{minipage}

\subsection{Datenvorbereitung für die Sentiment Analysis}
Für die weitere Verarbeitung müssen die Tweets aufbereitet werden. Grundsätzlich spielt der gewählte Sentiment-Analysis-Ansatz eine entscheidende Rolle bei der Wahl der Datenvorbereitungsmassnahmen. Einige Massnahmen sind jedoch unabhängig des Sentiment-Analysis-Ansatzes vorzunehmen.

\subsubsection{Sprachfilterung}
Da sich die Arbeit auf die englische Sentiment Analysis beschränkt, werden alle nicht englisch verfassten Tweets aussortiert. Dazu werden die Metadaten der Tweets verwendet, die von der Twitter API zur Verfügung gestellt werden. Diese beinhalten ein Attribut für die Sprache, das für die Filterung verwendet wird.

\subsubsection{Noise reduction}
Wie in Kapitel~\ref{ch:opinionmining} bereits erwähnt, werden auf Microblogging-Plattformen auch automatisiert Inhalte veröffentlicht. Im Fall von Kryptowährungen sind dies beispielsweise sogenannte Preis-Bots. Diese Systeme verfassen z. B. stündlich einen Tweet, der die Kryptowährung und deren aktuellen Währungspreis veröffentlicht. Solche Tweets sind für die Sentiment Analysis nicht relevant und werden aussortiert. Diese Problemstellung mit automatisiert generierten Inhalten ist im Opinion-Mining-Bereich sehr verbreitet.

\begin{italicquotes}
  ``The reason for doing this is because of the prevalence of Twitter bots, many of which instanteneously disseminate tweets containing particular keywords. Not removing these tweets will cause the distribution of words in our training set to be skewed.'' \cite[Colianni et al., S. 2]{algorithmictrading}
\end{italicquotes}

\noindent In der Wissenschaft gibt es verschiedene Ansätze wie solche Tweets identifiziert und bereinigt werden. Die einen versuchen, Twitter-User als Bots zu identifizieren. Andere Ansätze halten sich rein an den Inhalt des Tweets. In dieser Arbeit wird ein simpler Ansatz verfolgt, der aus der Arbeit von Clianni et al. stammt \cite{algorithmictrading}. Dieser Ansatz bescheibt ein Vorgehen in zwei Schritten, die auf den gesamten Datensatz angewandt werden. Als erstes werden nicht-alphanumerische Zeichen sowie Verlinkungen in jedem Tweet entfernt. Im zweiten Schritt werden alle Duplikate aus dem gesamten Datensatz entfernt. Gerade die Tweets der zuvor genannten Preis-Bots können so eliminiert werden.
