\section{Machine Learning Sentiment-Klassifikationsmodell}\label{ch:ml-sentiment}
In diesem Kapitel wird die Implementierung eines Machine Learning Sentiment Klassifkationsmodells erläutert. Dazu wird ein Neuronales Netz verwendet, das mehrere Schichten hat. Durch die Mehrschichtigkeit spricht man auch von einem \textit{Deep-Learning-Modell}. Die Implementierung basiert auf den Werkzeugen \textit{Keras}, \textit{TensorFlow}, \textit{Gensim} und \textit{scikit-learn}, die für die Programmiersprache Python verfügbar sind. Als Quellen dienen \cite{textanalyticspython}, \cite{handsonmachinelearning} sowie \cite{kerasbook}.

\begin{figure}[ht]
  \begin{center}
  \includegraphics[width=9cm]{img/ml-overview}
  \end{center}
  \caption[Schematische Darstellung des Machine Learning Klassifizierers]{Schematische Darstellung des Machine Learning Klassifizierers}
  \label{fig:mlclassifier}
\end{figure}

\noindent Die Ausganglsage ist die selbe wie beim regelbasierten Klassifikationsmodell. Wie in \Fref{fig:mlclassifier} ersichtlich, soll ein Text in Form eines Tweets aufgrund des Inhaltes durch das Klassifikationsmodell in die Kategorien \textit{positiv}, \textit{neutral} oder \textit{negativ} eingeteilt werden. Mit dem Vorwissen, dass Neuronale Netze nur numerische Werte interpretieren können und ein Tweet-Text eine sequenzielle Datenstruktur aufweist, werden zwei Probleme ersichtlich:
\begin{enumerate}
  \setlength\itemsep{-0.5em}
  \item Wie können die Tweets als numerische Vektoren repräsentiert werden?
  \item Wie muss das Neuronale Netz aufgebaut sein um sequenzielle Daten zu interpretieren?
\end{enumerate}

\noindent Die zwei offenen Fragen werden in zwei separaten Schritten bearbeitet und beantwortet.  Im ersten Schritt wird ein Modell erstellt, mit dem ein Tweet-Text durch ein numerischen Vektor repräsentiert werden kann (das Word2Vec-Modell). Im zweiten Schritt wird ein Sentimen- Klassifikationsmodell in Form eines rekurrenten Neuronalen Netzes trainiert. Dieses Modell soll schlussendlich in der Lage sein, die Kryptowährungs-Tweets in positiv, neutral oder negativ zu klassifizieren.

\subsection{Übersicht}
Machine Learning zeichnet sich dadurch aus, dass von bestehenden Daten gelernt werden kann. Ob ein Tweet nun eine positive oder negative Stimmung enthält, kann man aus den bereits gesammelten Kryptowährungs-Tweets nicht erlernen. Es werden also gelabelte Daten benötigt, die zum Lernen des Modells verwendet werden können. Um den Supervised-Learning-Ansatz verfolgen zu können, müssen die Daten Informationen darüber enthalten, wie die Stimmung im entsprechenden Text ist. Optimalerweise sind diese Daten ebenfalls Tweets, im Idealfall sogar im Kontext von Kryptowährungen.

Relativ nahe an die optimalen Voraussetzungen kommt der Datensatz von \textit{Sentiment140} \cite{sentiment140} heran. Der Datensatz enthält 1.6 Millionen Tweets, die durch ein manuelles Vorgehen bereits in die Kategorien \textit{positiv} oder \textit{negativ} eingeteilt sind. Die Tweets wurden nicht zu einem spezifischen Thema gesammelt und haben somit leider nicht den Kontext von Kryptowährungen, was sicherlich nicht optimal ist.

\begin{figure}[ht]
  \begin{center}
  \includegraphics[width=4cm]{img/train-test-split}
  \end{center}
  \caption[Aufteilung des Datensatzes in Trainings- und Testdaten]{Aufteilung des Datensatzes in Trainings- und Testdaten}
  \label{fig:split}
\end{figure}

Der Datensatz wird für die weiteren Schritte in einen Trainings- und einen Test Datensatz aufgeteilt. Dies entspricht der üblichen Vorgehensweise in Machine-Learning-Projekten. Das Verhältnis ist dabei 4:1, wobei der grössere Datensatz für das Trainieren der Modelle verwendet wird. Dieser Schritt ist entscheidend, um die Performance des trainierten Klassifikationsmodells mit den Test- bzw. Validierungsdaten zu messen. Wie bereits angedeutet, werden zwei Modelle benötigt. Beide werden jeweils mit dem Trainings-Datensatz trainiert. Nur so können schlussendlich mit dem Testdatensatz ``neue'' Tweets simuliert werden.

Die nächsten Kapitel beschreiben schrittweise die Erstellung des Klassifikationsmodells. Zunächst werden in Kapitel~\ref{ch:ml-preprocessing} die Datenvorbereitungen erläutert, die spezifisch für den Machine-Learning-Ansatz angewandt werden. Kapitel~\ref{ch:wordembeddings} klärt die Frage, wie Texte in numerische Vektoren repräsentiert werden können. Dazu wird in Kapitel~\ref{ch:w2vmodell} ein eigenes Modell trainiert, das anschliessend in Kapitel~\ref{ch:rnn} eingebunden wird.

\begin{figure}[ht]
  \begin{center}
  \includegraphics[width=5cm]{img/word2vec-rnn}
  \end{center}
  \caption[Anwendung des Word2Vec-Modells]{Anwendung des Word2Vec-Modells}
  \label{fig:word2vec-rnn}
\end{figure}

\subsection{Datenvorbereitung}\label{ch:ml-preprocessing}
Die Datenvorbereitung (oder das Preprocessing) variiert bei Textdaten in hohem Masse. Die angewandten Schritte sind stark von Format, Struktur und Schreibweise des Textes abhängig. Aus den gesammelten Kryptowährungs-Tweet, sowie dem Trainingsdatensatz wird schnell ersichtlich, dass die Grammatik in den verfassten Texten meist eine untergeordnete Rolle spielt. Um gleichbedeutende Wörter in eine gemeinsame Form zu bringen, wird ein Stemmer eingesetzt. Sehr viele Tweets enthalten auch Verlinkungen, entweder auf Webseiten oder auf andere Twitter-Nutzer. Diese Informationen sind für die Sentiment Analysis nicht relevant und werden entfernt. Die Datenvorbereitung beinhaltet folgenden Schritte:

\begin{itemize}
  \setlength\itemsep{-0.2em}
  \item Entfernen von englischen Stopwörtern
  \item Entfernen von Links auf Webseiten und Twitter-Nutzern
  \item Entfernen von Sonderzeichen
\end{itemize}

\noindent Die Information, ob ein Tweet positiv oder negativ ist (\textit{labels}), wird im \textit{Sentiment140} Datensatz durch den Wert 0 für negativ oder 4 für positiv in der Spalte \textit{target} hinterlegt. Diese Labels werden zu den Werten 0 für negativ und 1 für positiv encodiert. Mit solchen binären Klassen kann später das Neuronale Netz besser umgehen.\\

\begin{minipage}{\linewidth}
\begin{lstlisting}[style=py,caption={Datenvorbereitung},label={lst:dataprep}]
  STOP_WORDS = nltk.corpus.stopwords.words("english")
  REGEX = "@\S+|https?:\S+|http?:\S|[^A-Za-z0-9]+"
  tokens = []

  text = re.sub(REGEX, ' ', str(text).lower()).strip()
  for token in text.split():
      if token not in STOP_WORDS:
          tokens.append(token)
  text = " ".join(tokens)
\end{lstlisting}
\end{minipage}

\noindent Die Datenvorbereitung wird auf den gesamten Datensatz, also Test- sowie Traningsdaten angewandt und muss ebenfalls bei neuen Tweets, bzw. den Kryptowährungs-Tweets vor der Klassifikation angewandt werden.

\subsection{Word-Embeddings-Modell mit Word2Vec}\label{ch:w2vmodell}
In diesem Schritt soll ein Word-Embeddings-Modell erstellt werden, das zu einem Wort einen numerischen Vektor liefert. Dieses Modell wird schlussendlich in das eigentliche Klassifikationsmodell eingebunden. Es wird ein Word2Vec-Modell mit den Trainingsdaten trainiert, das ein mehrdimensionales Vektorraummodell liefert, in dem die Wörter des Trainingsdatensatzes enthalten sind. Die Ausgangslage ist in ~\Fref{fig:w2v} schematisch dargestellt. 
\begin{figure}[ht]
  \begin{center}
  \includegraphics[width=10cm]{img/w2v-model}
  \end{center}
  \caption[Vorrgehensweise zur Erstellung des Word-Embeddings Modelles]{Schematische Darstellung des Vorgehens zur Erstellung des Word-Embeddings Modelles}
  \label{fig:w2v}
\end{figure}

\noindent Zuerst werden dazu die Tweet-Texte in Token-Vektoren transformiert, wobei ein Token einer zusammenhängenden Zeichenfolge (bzw. einem Wort) entspricht. Als nächstes wird das Word2Vec-Vokabular erzeugt. Dabei werden nur Wörter beachtet, die mehr als 10mal im Datensatz vorhanden sind. Danach kann das Word2Vec-Modell mit den Token-Vektoren trainiert werden. Während $32$ Durchläufe (auch \textit{Epochen} genannt) wird das Vektorraummodell gebildet. 
Die Arbeit von Mikolov et al. zeigt, dass der Skip-gram-Ansatz in Bezug auf semantische Wortbeziehungen besser performt als der CBOW-Ansatz. Der CBOW Ansatz wiederum performt besser bei syntaktischen Wortbeziehungen. Da die Tweets syntaktisch nicht immer korrekt verfasst sind und in Bezug auf Sentiment Analysis die semantische Wortbeziehung eine wichtigere Rolle spielt, wird der Skip-gram-Ansatz mit einem Wort-Fenster von $7$ Wörtern gewählt. Der Wert liegt somit zwischen den Werten von $5$ und $10$, welche Mikolov et al. in ihrer Arbeit verwenden. Es wird erwähnt, dass ein höherer Wert zwar die Qualität der Ergebnisse erhöht, jedoch auch die Berechnungs-Komplexität steigert. Zudem zeigt die Arbeit, dass grössere Vektordimensionen besser performen. Für den Skip-gram-Ansatz wird dort die Dimension von $300$ gewählt. Diese wird auch in der vorliegenden Arbeit übernommen \cite{word2vec}. In Auflistung~\ref{lst:w2v} ist die schematische Implementierung zu sehen.\\

\begin{minipage}{\linewidth}
\begin{lstlisting}[style=py,caption={Anwendung Word2Vec Modelles},label={lst:w2v}]
  W2V_SIZE = 300        %*\# Anazahl Dimensionen eines Wort-Vektors*)
  W2V_WINDOW = 7        %*\# Wort-Fenster welches beachtet werden soll*)
  W2V_EPOCH = 32        %*\# Anzahl der Durchläufe*)
  W2V_MIN_COUNT = 10    %*\# Wörter welche weniger oft vorkommen werden ignoriert*)

  documents = [_text.split() for _text in tweets_train.text]
  w2v_model = Word2Vec(size=W2V_SIZE, window=W2V_WINDOW, min_count=W2V_MIN_COUNT, sg=1, workers=8)
  w2v_model.build_vocab(documents)
  w2v_model.train(documents, total_examples=len(documents), epochs=W2V_EPOCH)
\end{lstlisting}
\end{minipage}

\subsection{RNN-Sentiment-Klassifikationsmodell}\label{ch:rnn}
In diesem Kapitel wird der Aufbau des RNN-Sentiment-Klassifikationsmodelles erläutert. Das Neuronale Netz wird mit vier Schichten (engl. \textit{layer}) aufgebaut. Da das Netz nebst der Eingabeschicht (\textit{input layer}) und Ausgabeschicht (\textit{output layer}) mehr als eine Schicht enthält, wird es als \textit{deep} bezeichnet. In~\Fref{fig:rnnsummary} sind die verwendeten vier Layer inkl. ihrer Dimensionen ersichtlich. Im Weiteren werden die vier Layer einzeln erläutert und deren Konfiguration wird beschrieben. Auflistung~\ref{lst:rnn} zeigt die Implementierung in vereinfachter Form.

\begin{figure}[ht]
  \begin{center}
  \includegraphics[width=8cm]{img/model-summary}
  \end{center}
  \caption[Zusammenfassung des RNN Modelles]{Zusammenfassung des RNN Modelles}
  \label{fig:rnnsummary}
\end{figure}

\subsubsection{Embedding Layer}
Keras bietet einige Hilfestellungen im Umgang mit Textdaten an. Mit dem Keras Tokenizer wird ein Wortindex der Tweets aufgebaut. Somit erhält jedes Wort einen eigenen, numerischen Index. Dieser Tokenizer kann dann auf die Tweets angewandt werden und wandelt die Tweet-Texte in Nummerische Vektoren um, in dem jedes Wort durch den entsprechenden Index ersetzt wird. Die Tweets-Vektoren erhalten alle die selbe Grösse von 150 Elementen, wobei die leeren Elemente mit $0$ aufgefüllt werden. Somit entspricht ein solcher Vektor der Textsequenz eines Tweets und kann mithilfe des Keras Ebedding Layer bestens weiterverarbeitet werden.\\

\begin{minipage}{\linewidth}
\begin{lstlisting}[style=py,caption={Anwendung des Keras Tokenizer},label={lst:kerastokenizer}]
  SEQUENCE_LENGTH = 150       %*\# Vektorgrösse der Tokenizer Vektoren*)

  tokenizer = Tokenizer()
  tokenizer.fit_on_texts(tweets_train.text)
  feature_vectors = pad_sequences(tokenizer.texts_to_sequences(tweets_train.text), maxlen=SEQUENCE_LENGTH)
\end{lstlisting}
\end{minipage}

\noindent Der Embedding Layer dient nun dazu, die Tweets, die in einem Token-Index-Vektor auf das Neuronale Netz treffen, sequenziell in die entsprechende Vektorrepräsentation des Word2Vec-Modells jedes einzelnen Wortes zu wandeln. Dazu wird dem Ebedding Layer die Gewichtsmatrix mitgegeben, die der Zuweisung des Tokenizer-Index zum entsprechenden Word2Vec-Wortvektor entspricht. Diese Gewichte werden als nicht-trainierbar deklariert, da sie während des Trainings nicht verändert werden dürfen. Die Input-Dimension des Layers entspricht somit genau der Anzahl an Wörtern im Word2Vec-Modell. Die Output-Dimension der Vektorgrösse eines Wortvektors des Word2Vec-Word-Embeddings-Modells.

\subsubsection{Dropout Layer}\label{ch:dropoutlayer}
Der Dropout Layer implementiert ein Regulierungsverfahren für Neuronale Netze. Neuronale Netze werden regularisiert um Überanpassung (\textit{Overfitting}) zu vermeiden. Der Dropout Layer implementiert dieses Verfahren, indem er zufällig einzelne Ausgabewerte auf $0$ setzt. Somit kann beim Lernen die Wahrscheinlichkeit verringert werden, dass der Ausgabewert nur von einzelnen Eingabewerten abhängt. Mit der \textit{Dropoutquote} kann der Anteil an Neuronen bestimmt werden, die $0$ zurückliefern. Dass es durchaus Sinn macht, bereits den Word Embedding Layer zu regularisieren beschreiben Gal et al. \cite{dropout}.

\subsubsection{LSTM Layer}
Durch den \textit{``Long Short-Term Memory''} Layer wird das Neuronale Netz erst rekurrent (bzw. zu einem RNN). LSTM erweitert den Ansatz, dass bei einem RNN der Ausgabewert eines Neurons als Eingabewert für das zeitlich nächste Neuron verwendet wird. Durch die sogenannte \textit{Carry-Spur} können Ausgabewerte über ein längeres Zeitintervall statt bloss einem Zeitschritt gespeichert werden. Diese Carry-Spur verläuft über die Zeit und stellt den \textit{Carry-Wert} aus jeweils Eingabe-, Ausgabe- und vorherigem Carry-Wert zusammen \cite{kerasbook}.

\begin{figure}[ht]
  \begin{center}
  \includegraphics[width=11cm]{img/lstm}
  \end{center}
  \caption[Aufbau eines LSTM Layers \cite{kerasbook}]{Aufbau eines LSTM Layers. Chollet, S. 262, \cite{kerasbook}}
  \label{fig:lstm}
\end{figure}

\noindent Auch dieser Layer wird wieder mit dem Dropout-Verfahren reguliert. Wie der Auflistung~\ref{lst:rnn} zu entnehmen ist, werden dabei einerseits die Eingabewerte (Parameter \textit{dropout}) sowie die Werte des rekurrenten Zustandes (Parameter \textit{recurrent\textunderscore droput}) zufälligerweise auf $0$ gesetzt. Auch hier soll damit eine Überanpassung vermieden werden.

\subsubsection{Dense Layer}
Der Ausgangslayer hat schlussendlich nur noch eine Dimension von nur einem Neuron. Die Aktivierungsfunktion dieses Neuron beinhaltet die mathematische Sigmoid-Funktion, die Werte zwischen $0$ und $1$ zurückgibt. In Bezug auf die Problemstellung der Sentiment Analysis entspricht $0$ einem negativen Label bzw. $1$ einem positiven Label, wobei der Rückgabewert einer Wahrscheinlichkeit der Zuteilung zur entsprechenden Binärklasse entspricht.\\

\begin{minipage}{\linewidth}
\begin{lstlisting}[style=py,caption={Modell},label={lst:rnn}]
    model = Sequential()
    model.add(Embedding(vocab_size, W2V_SIZE, weights=[embedding_matrix], input_length=SEQUENCE_LENGTH, trainable=False))
    model.add(Dropout(0.5))
    model.add(LSTM(100, dropout=0.2, recurrent_dropout=0.2))
    model.add(Dense(1, activation='sigmoid'))
\end{lstlisting}
\end{minipage}

\subsection{Modell trainieren}
Das Training des Modells wird in 16 Durchläufen durchgeführt, wobei jeweils 1024 Datensätze verwendet werden. Da die Labels des Datensatzes binär sind (0 oder 1), wird die binäre Kreuzentropie (\textit{binary\textunderscore crossentropy}) als Verlustfunktion verwendet, die die Differenz zwischen der beobachteten Wahrscheinlichkeitsverteilung und der Vorhersage bemisst \cite{kerasbook}. Als Optimierer wird das stochastische Gradientenabstiegsverfahren namens \textit{adam} verwendet, da dies sich als robust und gut passend für eine grosse Vielfalt eignet, wie in der Arbeit beschrieben wird \cite{adam}.\\

\noindent\begin{minipage}{\linewidth}
  \begin{lstlisting}[style=py,caption={Trainieren des Modelles},label={lst:training}]
    EPOCHS = 16          %*\# Anzahl der Durchläufe*)
    BATCH_SIZE = 1024    %*\# Grösse eines Batches*)

    model.compile(loss='binary_crossentropy', optimizer="adam", metrics=['accuracy'])
    model.fit(x_train, y_train, batch_size=BATCH_SIZE, epochs=EPOCHS, validation_split=0.1, verbose=1)
  \end{lstlisting}
  \end{minipage}

\subsection{Klassifizierung der Tweets}
Wie in Kapitel~\ref{ch:dropoutlayer} bereits betont, liefert der Ausgangslayer des Modells eine Wahrscheinlichkeit der Klassenzuteilung zu positiv oder negativ. Ein Tweet, der nicht positiv oder negativ ist, soll allerdings wie beim regelbasierten Ansatz als neutral klassifiziert werden. Dazu werden Schwellenwerte gemäss Literatur bei $0.4$ und $0.7$ definiert. Liegt die vom Modell gelieferte Wahrscheinlichekeit innerhalb dieser Schwellenwerte, so wird der Tweet als \textit{NEUTRAL} klassifiziert. Ist der Wert über oder gleich $0.7$, so wird er als \textit{POSITIV} bzw. bei einem Wert unter oder gleich $0.4$ als \textit{NEGATIV} klassifiziert.\\

\noindent\begin{minipage}{\linewidth}
\begin{lstlisting}[style=py,caption={Klassifizierung eines einzelnen Tweets},label={lst:mlclassification}]
  x_test = pad_sequences(tokenizer.texts_to_sequences([text]), maxlen=SEQUENCE_LENGTH)
  score = model.predict([x_test])[0]

  label = NEUTRAL
  if score <= 0.4:
      label = NEGATIVE
  elif score >= 0.7:
      label = POSITIVE
\end{lstlisting}
\end{minipage}